import requests

# Replace 'YOUR_RAPIDAPI_KEY' with your actual RapidAPI key
RAPIDAPI_KEY = 'YOUR_RAPIDAPI_KEY'
BASE_URL = 'https://imdb8.p.rapidapi.com/'

def search_movies(query):
    headers = {
        'X-RapidAPI-Key': RAPIDAPI_KEY,
        'X-RapidAPI-Host': 'imdb8.p.rapidapi.com',
    }

    endpoint = 'title/find'

    params = {'q': query}

    try:
        response = requests.get(BASE_URL + endpoint, headers=headers, params=params)
        response.raise_for_status()
        data = response.json()

        if isinstance(data, list) and data:
            first_result = data[0]
            title = first_result.get('title', 'N/A')
            year = first_result.get('year', 'N/A')
            print(f"Title: {title}, Year: {year}")
        else:
            print("No results found.")

    except requests.exceptions.RequestException as e:
        print(f"Error: {e}")

# Example usage
search_query = 'Game of Thrones'
search_movies(search_query)

